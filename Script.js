        function listOnePhoto(){
            fetch("https://jsonplaceholder.typicode.com/photos/1")
            .then(function(response){
                return response.json()
            })
            .then((response) => {
                console.log(response)
                var result = document.getElementById('result')                
                
                //GET IMAGE URL
                src = response.url

                //DISPLAY IMAGE IN WEB BROWSER
                img = document.createElement('img');
                img.src = src;
                document.body.appendChild(img);
            })
        }

         /*AC2 - User is able to visualize all photos from a gallery
        ----
        For the moment the gallery is hardcoded to the value '1' to validate the delete feature
        ----
        To be replace with an identifier:
        *    function listAllPhoto(int id){
        *       fetch("https://jsonplaceholder.typicode.com/albums/" + id + "/photos"

        */
        function listAllPhoto(){
            fetch("https://jsonplaceholder.typicode.com/albums/1/photos")
            .then(function(response){
                return response.json()
            })
            .then((response) => {

                /*used for appending all request results to one variable*/
                var requestResult = ''
                
                /*PARSE HTML STRING TO JS TO DISPLAY ALL IMAGES */
                /*
                var htmlString = "<hmtl><body>  <a href='response.url'></a>   </body></html>"
                var parser = new DOMParser();
                var doc = parser.parseFromString(htmlString, "text/html")
                */
                /* 
                var imageHolder = document.createElement('html')
                imageHolder.innerHTML = `<hmtl><body>  <a href='response.url'></a>   </body></html>`        
                */
                
                //imageHolder.getElementsByTagName( 'a' );

                //LISTS
                response.forEach(element => {

                    requestResult = element.title + " " + element.url;
                    result.append(requestResult)
                })
                //result.innerHTML = response.url

            }
            )            
        }

        /*Lists the IDENTIFIER and TITLE of every gallery in the DB*/
        function listAllGallery(){
            fetch("https://jsonplaceholder.typicode.com/albums")
            .then(function(response){
                return response.json()
            })
            .then((response) => {
                console.log(response)
                var requestResult = ''
                
                response.forEach(element => {

                    requestResult = "Gallery " + element.id + ": " + element.title;
                    result.append(requestResult)
                })
            }
            )            
        }

        /*AC1 - User is able to delete a gallery
        ----
        For the moment the gallery is hardcoded to the value '1' to validate the delete feature
        ----
        To be replace with an identifier:
        *    function deleteGallery(int id){
        *       fetch("https://jsonplaceholder.typicode.com/albums/" + id ....

        */
        function deleteGallery(){
            fetch('https://jsonplaceholder.typicode.com/albums/1', {
                method: 'DELETE',
            });
        }
